# 1 关于Phytium-Xen

Xen是Type-1类型的虚拟机管理器(Hypervisor),官方网址为:https://xenproject.org

Phytium-Xen针对飞腾嵌入式系列芯片对Xen做了相应适配和优化，官方网址为:https://gitee.com/phytium_embedded/phytium-xen

Phytium-Xen当前支持的芯片: E2000Q（包括飞腾派产品）

Phytium-Xen当前支持的引导: Uboot

Phytium-Xen当前支持的linux内核：4.19、5.10

Xen提供了在同一台计算机上承载多个操作系统的能力。Xen可以运行多个客户机操作系统，每个客户机操作系统与Xen交互，就像它与真实硬件交互一样。Xen为每个虚拟机创建内存分区，其他虚拟机无法访问本机内存，除非被授予特殊权限。Xen也控制虚拟机占用的CPU核数和时间等资源，防止虚拟机占用所有的系统资源。

作为系统中最底层的软件，Xen一般通过U-Boot这样的引导程序加载运行。初始化之后，Xen立即启动第一个虚拟机，称为dom0。dom0在系统中充当Xen的代理，是管理程序操作和启动所必需的。通过dom0，系统开发人员可以控制虚拟机的启动、暂停或关闭。默认情况下，dom0可以访问所有系统资源。dom0以外的虚拟机通常称为domu。


# 2 软硬件资源

## 2.1 硬件资源

飞腾E2000Q系列硬件平台，详情请咨询飞腾技术支持或FAE

## 2.2 软件资源

linux内核源码: https://gitee.com/phytium_embedded/phytium-linux-kernel

操作系统版本: Ubuntu 20.04

构建操作系统: https://gitee.com/phytium_embedded/phytium-linux-buildroot

获取操作系统Image: 详情请咨询飞腾技术支持或FAE


# 3 编译linux内核

## 3.1 下载Phytium-Linux-Kernel源代码

    # git clone https://gitee.com/phytium_embedded/phytium-linux-kernel
    # git branch -a
    # git checkout origin/linux-5.10    /* 注意：这里假定内核版本为5.10 */
    # cd phytium-linux-kernel

## 3.2 确认内核配置文件

    # vim arch/arm64/configs/phytium_defconfig
 
请确认如下选项:

    CONFIG_XEN_DOM0=y
    CONFIG_XEN=y
    CONFIG_XEN_BLKDEV_FRONTEND=y
    CONFIG_XEN_BLKDEV_BACKEND=y
    CONFIG_XEN_NETDEV_FRONTEND=y
    CONFIG_XEN_NETDEV_BACKEND=y
    CONFIG_INPUT_XEN_KBDDEV_FRONTEND=y
    CONFIG_HVC_XEN=y
    CONFIG_HVC_XEN_FRONTEND=y
    CONFIG_XEN_FBDEV_FRONTEND=y
    CONFIG_XEN_BALLOON=y
    CONFIG_XEN_SCRUB_PAGES_DEFAULT=y
    CONFIG_XEN_DEV_EVTCHN=y
    CONFIG_XEN_BACKEND=y
    CONFIG_XENFS=y
    CONFIG_XEN_COMPAT_XENFS=y
    CONFIG_XEN_SYS_HYPERVISOR=y
    CONFIG_XEN_XENBUS_FRONTEND=y
    CONFIG_XEN_GNTDEV=y
    CONFIG_XEN_GRANT_DEV_ALLOC=y
    CONFIG_SWIOTLB_XEN=y
    CONFIG_XEN_PRIVCMD=y
    CONFIG_XEN_EFI=y
    CONFIG_XEN_AUTO_XLATE=y

## 3.3 修改内核设备树

    # vim arch/arm64/boot/dts/phytium/e2000q-demo-board.dts
 
如下更新chosen节点:

    chosen {
        stdout-path = "serial1:115200n8";

        /* 注:下面的信息为xen相关的配置，其中“root=”选项配置根据本地环境进行调整，比如飞腾派使用SD卡做为存储器，则root=/dev/mmcblk0p1等等 */
        xen,dom0-bootargs = "console=ttyAMA1,115200 earlycon=pl011,0x2800d000 root=/dev/sda2 pci=nomsi rw";
        xen,xen-bootargs = "console=dtuart dtuart=/soc/uart@2800d000  hmp-unsafe=true cpufreq=dom0-kernel cpus=[0-1, 2-3]";
        #size-cells = <0x00000001>;
        #address-cells = <0x00000001>;

        module@0 {
            reg = <0x91000000 0x03000000>;
            compatible = "xen,linux-zimage", "xen,multiboot-module";
        };
    };

## 3.4 编译内核

    # make phytium_defconfig
    # make -j4

## 3.5 检查内核镜像和设备树

    # ls arch/arm64/boot/Image
    # ls arch/arm64/boot/dts/phytium/e2000q-demo-board.dtb

## 3.6 拷贝镜像和设备树到磁盘引导分区

    # mount /dev/sda1 /mnt    /* 注: 这里假定引导分区为sda1 */
    # cp arch/arm64/boot/Image  /mnt
    # cp arch/arm64/boot/dts/phytium/e2000q-demo-board.dtb  /mnt
    # umount /mnt

## 3.7 设备树预留Xen节点信息的空间
 
**注意: 如果执行了3.3节，则忽略本步骤！**

    # mount /dev/sda1  /mnt
    # cd /mnt
    # dtc -I dtb -O dts e2000q-demo-board.dtb > tmp.dts
    # dtc -I dts -O dtb -S 0x8000 tmp.dts > e2000q-demo-board.dtb
    # umount /mnt


# 4 编译Phytium-Xen

**注意: 因为在后续使用Xen时，依赖Xen的相关工具，为了方便后续调试，建议按照此章节在测试板上搭建编译/调试环境！**

## 4.1 安装基础包

    # apt install -y python3-dev acpica-tools uuid-dev libncurses5-dev libpixman-1-dev libglib2.0-dev
    # apt install -y libyajl-dev libfdt-dev flex bison device-tree-compiler bridge-utils

## 4.2 下载Phyitum-Xen源代码

    # git clone https://gitee.com/phytium_embedded/phytium-xen.git
    # cd src/xen

## 4.3 运行configure

    # ./configure --enable-xen --enable-tools
  
## 4.4 修改配置文件

    # vim config/Tools.mk
    

找到CONFIG_QEMU_XEN配置，修改为:

    CONFIG_QEMU_XEN     := n

## 4.5 编译与安装

    # make -j4
    # make install

## 4.6 检查xen镜像

    # ls xen/xen

![输入图片说明](pic/xen_img.png)

## 4.7 拷贝xen镜像到磁盘引导分区

    # mount /dev/sda1  /mnt    /* 注: 这里假定引导分区为sda1 */
    # cp xen/xen  /mnt
    # umount /mnt


# 5 运行Phytium-Xen

## 5.1 Uboot引导

### 5.1.1 重启系统

按电源开关重启系统，系统将自动进入到Uboot的命令行界面

### 5.1.2 设备树添加xen信息
 
**注意: 如果执行了3.3节，则忽略本步骤！** 

Uboot命令行依次输入如下命令，添加xen信息:

    # fdt addr 0x90000000;
    # fdt set /chosen \#address-cells <1>;
    # fdt set /chosen \#size-cells <1>;
    # fdt set /chosen xen,xen-bootargs "console=dtuart dtuart=/soc/uart@2800d000  hmp-unsafe=true cpus = ["0-1", "2-3"]"; 
    # fdt set /chosen xen,dom0-bootargs "console=ttyAMA1,115200 earlycon=pl011,0x2800d000 root=/dev/sda2 rw";
    # fdt mknod /chosen module@0;
    # fdt set /chosen/module@0 compatible "xen,linux-zimage" "xen,multiboot-module";
    # fdt set /chosen/module@0 reg <0x91000000 0x3000000>;

### 5.1.3 引导Phytium-Xen

Uboot命令行依次输入如下命令，引导Phytium-Xen:

    # ext4load scsi 0:1 0x90000000 e2000q-demo-board.dtb;
    # ext4load scsi 0:1 0x91000000 Image;
    # ext4load scsi 0:1 0x95000000 xen;
    # booti 0x95000000 - 0x90000000
    
注意：上述三个镜像均需提前放到磁盘的引导分区，见3.6节、4.7节

### 5.1.4 登录Phytium-Xen

下图是Xen的启动过程和dom0的登录界面：

![输入图片说明](pic/xen_boot.png)

![输入图片说明](pic/dom0_boot.png)


## 5.2 Dom0系统工具配置

Dom0启动后，需要挂载Xen相关的文件系统和启动守护进程，之后才能使用xl工具查看Xen信息，动态创建和关闭虚拟机等功能，启动脚本environ.sh内容如下所示：

    mkdir -p /var/lib/xenstored

    mount -t xenfs xenfs /proc/xen
    mount -t tmpfs xenstore /var/lib/xenstored

    mkdir -p /var/run/xen

    xenstored --pid-file /var/run/xen/xenstored.pid
    xenconsoled -i --log=none --log-dir=/var/log/xen/console &

    xenstore-write domid 0

    /usr/local/lib/xen/bin/xen-init-dom0

系统启动后使用如下命令启动即可：

    # source environ.sh

通过xl工具命令打印Xen系统信息如下图所示:

![输入图片说明](pic/xl_info.png)

打印Xen中的Domain信息：

![输入图片说明](pic/xl_list.png)

注：在路径binary及linux-domu下，已存放demo测试用的各个文件，仅做demo参考。binary路径下，“xen”文件为xen镜像文件，基于xen4.17.2版本编译而来；“xen-e2000q.dtb”为uboot启动引导的设备树文件。“linux-domu”路径下，“Image”文件为dom0和domu使用的内核镜像文件。设备树文件和内核镜像文件均基于gitee上e2000q 4.19的内核源码编译，tag点为“kernel_4.19-v1.1”。

## 5.3 创建并运行Linux domu

创建配置文件linux.cfg，内容如下:

    name="domu"
    memory=512
    kernel="./Image"
    ramdisk="./initrd.ext2"
    extra="root=/dev/ram rdinit=/linuxrc console=ttyAMA0 ramdisk_size=0x1000000"

通过xl命令创建并运行Linux domu:

    # xl create linux.cfg

查看现有的domain并连接到domu的控制台，效果如下图所示，可通过 " CTRL + ] " 退出domu的控制台：

![输入图片说明](pic/create_linux_domu.png)

![输入图片说明](pic/console_domu.png)

![输入图片说明](pic/linux_domu_cmd.png)

销毁domain命令如下：

    # xl destroy domu

## 5.4 创建虚拟机网卡

目前支持使用桥接模式和NAT模式来配置虚拟机虚拟网卡，下面给出配置示例。

### 5.4.1 桥接模式

#### 5.4.1.1 dom0相关配置

首先，需要在dom0下创建网桥:

	# brctl addbr xenbr0
    # brctl addif xenbr0 eth0		/* 注: 这里假定物理网络端口为eth0，请根据实际配置 */

为网桥xenbr0配置mac地址、ip、netmask和broadcast：

    # ip link set dev xenbr0 address 00:23:34:54:55:95
    # ifconfig xenbr0 10.31.90.246 netmask 255.255.255.0 broadcast 10.31.90.255			/* 注: IP地址请根据实际配置 */

因为我们将dom0的网络端口eth0添加到Linux桥接设备xenbr0中，这将使eth0成为xenbr0的一个端口，需要进行简单处理，之后可与局域网连接：

    # ifconfig eth0 down
    # ifconfig eth0 up

![输入图片说明](pic/dom0_net.png)

如果需要与外网连接，需要修改路由如下：

    # ip route change default via 10.31.90.254 dev xenbr0

![输入图片说明](pic/dom0_route.png)

#### 5.4.1.2 domu相关配置

在创建domu的配置文件中加入：

	vif=["bridge=xenbr0"]

这样创建的虚拟机中会出现虚拟网口eth0，为eth0配置ip、netmask和broadcast后可与局域网连接。

![输入图片说明](pic/domu_eth0.png)

如果需要与外网连接，需要添加路由如下：

    # ip route add default via 10.31.90.254 dev eth0

### 5.4.2 NAT模式

与桥接模式不一样，NAT模式不需要创建网桥，但是在创建建虚拟机的配置文件中需要指定Xen的自有vif-nat脚本（脚本位于/etc/xen/scripts/vif-nat）：

    # vif=[“script=vif-nat”]

网络配置方面，基本操作与桥接模式基本一致，组网按实际情况进行，不在此赘述。

### 5.4.3 双向路由网络模式

此模式也不需要创建网桥，但是在创建建虚拟机的配置文件中需要指定Xen的自有vif-route脚本（脚本位于/etc/xen/scripts/vif-route）：

    # vif=[“script=vif-route”]

网络配置方面，基本操作与桥接模式基本一致，组网按实际情况进行，不在此赘述。

## 5.5 创建虚拟机磁盘

在Xen创建虚拟机配置文件中可以加入disk参数，从而指定虚拟机将要使用的磁盘设备的配置信息，下面给出示例作为参考。

首先，通过命令“dd if=/dev/zero of=/root/for_xen/a1.img bs=1M count=3000”生成一个数据全0的镜像文件。然后在虚拟机配置文件中加入disk参数指定虚拟机磁盘：

    # disk=['format=raw, vdev=xvda, access=w, target=/root/for_xen/a1.img']

效果如下所示：

![输入图片说明](pic/disk_config.png)

创建虚拟机后可在虚拟机domu中看到对应磁盘：

![输入图片说明](pic/disk_in_domu.png)

## 5.6 创建并运行Vxworks domu

创建配置文件vx.cfg，内容如下:

    name = "vxworks-dom"
    kernel = "./zVxWorks.bin"
    memory = 512
    vcpus = 2
    vif = ['bridge=xenbr0']

运行VxWorks domu并连接到控制台，命令如下：

    # xl create -c vx.cfg

效果如下图所示：

![输入图片说明](pic/vxworks.png)

# 6 其他

如有需要，可联系飞腾嵌入式软件部,联系邮箱：

    yangshaojun@phytium.com.cn
    huyuming1672@phytium.com.cn
