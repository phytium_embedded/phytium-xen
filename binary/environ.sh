mkdir -p /var/lib/xenstored

mount -t xenfs xenfs /proc/xen
mount -t tmpfs xenstore /var/lib/xenstored

mkdir -p /var/run/xen

xenstored --pid-file /var/run/xen/xenstored.pid
xenconsoled -i --log=none --log-dir=/var/log/xen/console &

xenstore-write domid 0

/usr/local/lib/xen/bin/xen-init-dom0
